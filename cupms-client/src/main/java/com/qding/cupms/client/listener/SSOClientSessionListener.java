package com.qding.cupms.client.listener;

import org.apache.shiro.cache.CacheManager;
import org.apache.shiro.session.Session;
import org.apache.shiro.session.SessionListener;
import org.apache.shiro.session.mgt.SimpleSession;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;


/**
 * <一句话功能简述>
 * sso-client 会话监听
 * @since  [产品/模块版本]
 * @author ly
 * @version v1.0
 * Date:2017年8月9日 下午3:30:38
 */
public class SSOClientSessionListener implements SessionListener {

    Logger logger = LoggerFactory.getLogger(SSOClientSessionListener.class);
    
    public void onStart(Session session) {
        logger.debug("session start " + session.getId());
    }

    public void onStop(Session session) {
        logger.debug("session stop " + session.getId());
    }

    public void onExpiration(Session session) {
        logger.info("session expiration "+session.getId());
    }

 
}
