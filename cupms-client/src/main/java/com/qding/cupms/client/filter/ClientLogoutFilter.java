package com.qding.cupms.client.filter;

import javax.servlet.ServletRequest;
import javax.servlet.ServletResponse;

import org.apache.shiro.session.SessionException;
import org.apache.shiro.subject.Subject;
import org.apache.shiro.web.filter.authc.LogoutFilter;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.qding.cupms.client.common.Helper;
import com.qding.cupms.common.model.AnswerMsg;
import com.qding.cupms.common.model.AnswerMsgFactoryImpl;


/**
 * 登录filter
 * 登录filter
 * @since  [产品/模块版本]
 * @author ly
 * @version v1.0
 * Date:2017年8月10日 上午9:57:42
 */
public class ClientLogoutFilter extends LogoutFilter {
    
   private static final  Logger  _log=LoggerFactory.getLogger(ClientLogoutFilter.class);
    
    /** 
     * 注销处理
     * 注销处理
     * @param request  
     * @param response 
     * @return
     * @throws Exception
     * @author Administrator
     * Date:2017年8月10日 上午9:59:03
     */
    @Override
    protected boolean preHandle(ServletRequest request, ServletResponse response) throws Exception {
        _log.debug("调用了注销filter");
        
        
        /**向认证中发起注册请求**/
        /**向认证中发起注册请求**/
        
        AnswerMsg  answerMsg=AnswerMsgFactoryImpl.createSuccessAnserMsg("注销成功!");
        Helper.writeResponse(response, answerMsg);
        return false;
    }

}
