package com.qding.cupms.client.filter;

import java.io.IOException;

import javax.servlet.ServletRequest;
import javax.servlet.ServletResponse;
import javax.servlet.http.HttpServletRequest;

import org.apache.commons.lang3.StringUtils;
import org.apache.shiro.web.filter.authc.AuthenticationFilter;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.redis.core.StringRedisTemplate;

import com.qding.cupms.client.common.Helper;
import com.qding.cupms.client.common.ServerHelper;
import com.qding.cupms.client.session.ClientTokenAuthentication;
import com.qding.cupms.common.constant.Constant;

/**
 * 认证filter
 * 
 * @since [产品/模块版本]
 * @author ly
 * @version v1.0 Date:2017年8月9日 下午3:43:02
 */
public class ClientAuthenticationFilter extends AuthenticationFilter {

    private final static Logger _log = LoggerFactory.getLogger(ClientAuthenticationFilter.class);

    @Autowired
    private ClientTokenAuthentication clientTokenAuthentication;

    @Override
    protected boolean isAccessAllowed(ServletRequest request, ServletResponse response, Object mappedValue) {
        _log.debug("ClientAuthenticationFilter.isAccessAllowed::" + "  Filter 调用!");

        return validateClient(request, response);
    }

    @Override
    protected boolean onAccessDenied(ServletRequest request, ServletResponse response) throws Exception {
        _log.debug("ClientAuthenticationFilter.onAccessDenied::" + "  Filter 调用!");
        return false;
    }

    /**
     * 校验令牌
     * 
     * @param token
     *            令牌
     * @return
     * @author ly Date:2017年8月11日 下午5:37:24
     */
    private boolean validateToken(String token) {
        return true;
    }

    /**
     * 认证client请求 <功能详细描述>
     * 
     * @param request
     * @param response
     * @return true 通过继续下一步 , false 不通过终
     * @author ly Date:2017年8月10日 上午10:53:57
     * @throws IOException
     */
    private boolean validateClient(ServletRequest request, ServletResponse response) {

        try {
            HttpServletRequest httpServletRequest = (HttpServletRequest) request;
            String headerTokenName = Constant.HEADER_TOKEN_NAME;
            String token = httpServletRequest.getHeader(headerTokenName);

            if (StringUtils.isBlank(token)) {
                // 没有 token 返回登录报文
                Helper.writeNologinResponse(response);
                return false;
            }

            if (!clientTokenAuthentication.checkTokenByLocal(token)) {
                // 局部会话校验没通过
                
                if(clientTokenAuthentication.checkTokenByRemoting(token)==null){
                    //远程校验token也没通过
                    Helper.writeSessionTimeOutResponse(response);
                    //到认证中心注销
                    ServerHelper.checkToken(token);
                    return false;
                }else{
                	//创建局部会话
                	
                }
            }else{
            	
            	
            }
            
            return true;
        } catch (Exception e) {
            _log.error(e.getMessage(), e);
            try {
                Helper.writeSSOSysErrorResponse(response);
            } catch (IOException e1) {
                e1.printStackTrace();
            }
            
            return false;
            
        }
    }

}
