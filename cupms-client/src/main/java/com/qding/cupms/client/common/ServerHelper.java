package com.qding.cupms.client.common;

import java.io.IOException;
import java.io.PrintWriter;

import javax.servlet.ServletResponse;

import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.JSONObject;
import com.qding.cupms.common.constant.Constant;
import com.qding.cupms.common.http.SimpleHttpParam;
import com.qding.cupms.common.http.SimpleHttpResult;
import com.qding.cupms.common.http.SimpleHttpUtils;
import com.qding.cupms.common.model.AnswerMsg;
import com.qding.cupms.common.model.AnswerMsgFactoryImpl;
import com.qding.cupms.common.model.CheckTokenDTO;
import com.qding.cupms.common.model.ResponseCodeEnum;
import com.qding.cupms.common.model.SSOClientInfo;
import com.qding.cupms.common.utils.JsonUtil;
import com.qding.cupms.common.utils.PropertiesFileUtil;

/**
 * 服务端端代理类
 * 
 * @since [基础数据/单点登录]
 * @author ly
 * @version v1.0 Date:2017年8月11日 上午11:09:58
 */
public class ServerHelper {

	private final static Logger _log = LoggerFactory.getLogger(ServerHelper.class);

	/**
	 * 服务端登出
	 * 
	 * @param token
	 *            令牌
	 * @return
	 * @author ly Date:2017年8月11日 上午11:57:57
	 */
	public static CheckTokenDTO checkToken(String token) {
		try {

			String serverUri = PropertiesFileUtil.getInstance(Constant.CLIENT_PROPERTIES)
					.get("CLIENT_PROPERTIES_SERVERURI");
			String realUri = SimpleHttpUtils.createUrlAddress(serverUri, Constant.SERVER_SSO_CHECKTOKEN);

			SimpleHttpParam httpParam = new SimpleHttpParam(realUri);
			httpParam.addHeader(Constant.HEADER_TOKEN_NAME, token);
			_log.info("url:{},header:{}", realUri, httpParam.getHeaders());

			SimpleHttpResult httpResult = SimpleHttpUtils.httpRequest(httpParam);
			_log.info("httpResult:{}", httpResult.toString());

			String content = httpResult.getContent();
			JSONObject jsonObj = JsonUtil.JsonStr2Java(content, JSONObject.class);

			JSONObject dataJson = jsonObj.getJSONObject("data");

			CheckTokenDTO checkTokenDTO = JSON.toJavaObject(dataJson, CheckTokenDTO.class);

			return checkTokenDTO;
		} catch (Exception e) {
			_log.error(e.getMessage(), e);
			return null;
		}

	}

	/**
	 * 服务端注销
	 * 
	 * @param token
	 * @return
	 */
	public static boolean cancel(String token) {
		try {

			String serverUri = PropertiesFileUtil.getInstance(Constant.CLIENT_PROPERTIES)
					.get("CLIENT_PROPERTIES_SERVERURI");
			String realUri = SimpleHttpUtils.createUrlAddress(serverUri, Constant.SERVER_SSO_CANCEL);

			SimpleHttpParam httpParam = new SimpleHttpParam(realUri);
			httpParam.addHeader(Constant.HEADER_TOKEN_NAME, token);
			_log.info("url:{},header:{}", realUri, httpParam.getHeaders());

			SimpleHttpResult httpResult = SimpleHttpUtils.httpRequest(httpParam);
			_log.info("httpResult:{}", httpResult.toString());

			String content = httpResult.getContent();
			if(StringUtils.isBlank(content)){
				return false;
			}
			return true;
		} catch (Exception e) {
			_log.error(e.getMessage(), e);
			return false;
		}

	}

}
