package com.qding.cupms.client.session;

import org.apache.commons.lang3.StringUtils;
import org.apache.shiro.session.Session;
import org.apache.shiro.session.UnknownSessionException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.redis.core.StringRedisTemplate;

import com.qding.cupms.client.common.ServerHelper;
import com.qding.cupms.common.constant.Constant;
import com.qding.cupms.common.model.CheckTokenDTO;

/**
 * local token 校验
 * 
 * @since [基础数据/单点登录]
 * @author ly
 * @version v1.0 Date:2017年8月11日 下午5:48:08
 */
public class ClientTokenAuthentication {

    @Autowired
    private StringRedisTemplate redisTemplate;

    @Autowired
    private ClientShiroRedisSessionDao clientShiroRedisSessionDao;

    /**
     * 校验token
     * 
     * @param token
     *            令牌
     * @return true 通过校验 ,false 不通过校验
     * @author ly Date:2017年8月11日 下午5:50:57
     */
    public boolean checkTokenByLocal(String token) {

        // 通过token查询sessionId
        String sessionId = (String) redisTemplate.opsForHash().get(Constant.CLIENT_TOKEN_REDIS_HASH_KEY, token);
        if (StringUtils.isBlank(sessionId)) {
            return false;
        }
        // UnknownSessionException

        try {
            Session session = clientShiroRedisSessionDao.doReadSession(sessionId);
            
            if(session.getAttribute(Constant.CLIENT_USER_SIGN)==null){
                //没有用户信息
                return false;
            }
        } catch (UnknownSessionException e) {
            return false;
        }
        return true;
    }

    /**
     * 运程校验令牌
     * 
     * @param token
     *            令牌
     * @return
     * @author ly Date:2017年8月11日 下午5:54:56
     */
    public CheckTokenDTO checkTokenByRemoting(String token) {
    	CheckTokenDTO  checkTokenDTO=ServerHelper.checkToken(token);
        return checkTokenDTO;
    }

}
