package com.qding.cupms.client.common;

import java.io.IOException;
import java.io.PrintWriter;

import javax.servlet.ServletResponse;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.qding.cupms.common.constant.Constant;
import com.qding.cupms.common.model.AnswerMsg;
import com.qding.cupms.common.model.AnswerMsgFactoryImpl;
import com.qding.cupms.common.model.ResponseCodeEnum;
import com.qding.cupms.common.utils.JsonUtil;

/**
 * 一些帮助类
 * 
 * @since [产品/模块版本]
 * @author Administrator
 * @version v1.0 Date:2017年8月10日 上午10:16:35
 */
public class Helper {

    private final static Logger _log = LoggerFactory.getLogger(Helper.class);

    /**
     * 向response 回写报文 向response 回写报文
     * 
     * @param response
     *            请求响应
     * @param answerMsg
     *            报文
     * @author ly Date:2017年8月10日 上午10:17:43
     * @throws IOException
     */
    public static void writeResponse(ServletResponse response, AnswerMsg answerMsg) throws IOException {
        PrintWriter out = null;
        try {
            String msg = JsonUtil.Java2Json(answerMsg);
            response.setCharacterEncoding(Constant.RESPONSE_CHARACTER_ENCODING);
            response.setContentType(Constant.RESPONSE_CONTENTTYPE);
            out = response.getWriter();
            out.write(msg);
            out.flush();
        } finally {
            if (out != null) {
                out.close();
            }
        }

    }

    /**
     * 未登录响应报文
     * 
     * @param response
     * @throws IOException
     * @author Administrator Date:2017年8月10日 上午11:27:55
     */
    public static boolean writeNologinResponse(ServletResponse response) throws IOException {
        PrintWriter out = null;
        try {
            AnswerMsg answerMsg = AnswerMsgFactoryImpl.createErrorAnserMsg(ResponseCodeEnum.AUTH.getValue(), "请先登录");
            String msg = JsonUtil.Java2Json(answerMsg);
            response.setCharacterEncoding(Constant.RESPONSE_CHARACTER_ENCODING);
            response.setContentType(Constant.RESPONSE_CONTENTTYPE);
            out = response.getWriter();
            out.write(msg);
            out.flush();
        } finally {
            if (out != null) {
                out.close();
            }
        }

        return true;

    }
    
    
    public static boolean writeSessionTimeOutResponse(ServletResponse response) throws IOException {
        PrintWriter out = null;
        try {
            AnswerMsg answerMsg = AnswerMsgFactoryImpl.createErrorAnserMsg(ResponseCodeEnum.AUTH.getValue(), "会话失效");
            String msg = JsonUtil.Java2Json(answerMsg);
            response.setCharacterEncoding(Constant.RESPONSE_CHARACTER_ENCODING);
            response.setContentType(Constant.RESPONSE_CONTENTTYPE);
            out = response.getWriter();
            out.write(msg);
            out.flush();
        } finally {
            if (out != null) {
                out.close();
            }
        }

        return true;

    }
    
    /**
     * 系统异常响应报文
     * 
     * @param response
     * @throws IOException
     * @author Administrator Date:2017年8月10日 上午11:27:55
     */
    public static boolean writeSSOSysErrorResponse(ServletResponse response) throws IOException {
        PrintWriter out = null;
        try {
            AnswerMsg answerMsg = AnswerMsgFactoryImpl.createErrorAnserMsg(ResponseCodeEnum.SYSTEM_ERROR.getValue(), "SSO系统异常");
            String msg = JsonUtil.Java2Json(answerMsg);
            response.setCharacterEncoding(Constant.RESPONSE_CHARACTER_ENCODING);
            response.setContentType(Constant.RESPONSE_CONTENTTYPE);
            out = response.getWriter();
            out.write(msg);
            out.flush();
        } finally {
            if (out != null) {
                out.close();
            }
        }

        return true;

    }

}
