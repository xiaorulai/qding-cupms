package com.qding.cupms.admin.rest.controller.manage;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import com.qding.cupms.admin.rest.controller.BaseController;
import com.qding.cupms.common.model.AnswerMsg;
import com.qding.cupms.common.model.AnswerMsgFactoryImpl;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;

/**
 * 角色controller
 * Created by shuzheng on 2017/2/6.
 */
@Controller
@Api(value = "角色管理", description = "角色管理")
@RequestMapping("/manage/role")
public class ManageUpmsRoleController extends BaseController {

    private static Logger _log = LoggerFactory.getLogger(ManageUpmsRoleController.class);
    
    
    /** 
     * <功能详细描述>
     * @param id
     * @param modelMap
     * @return
     * @author Administrator
     * Date:2017年8月10日 下午3:06:23
     */
    @ApiOperation(value = "角色权限")
    @RequestMapping(value = "/permission/{id}", method = RequestMethod.GET)
    public AnswerMsg  permission(@PathVariable("id") int id, ModelMap modelMap) {
        return AnswerMsgFactoryImpl.createSuccessAnserMsg("OK");
    }

   
}
