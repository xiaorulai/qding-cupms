package com.qding.cupms.admin.rest.shiro;

import org.apache.shiro.session.Session;
import org.apache.shiro.session.mgt.SessionContext;
import org.apache.shiro.session.mgt.SessionFactory;
import org.apache.shiro.web.session.mgt.WebSessionContext;

import javax.servlet.http.HttpServletRequest;


/**
 * 全局session工厂
 * @since  [基础数据/单点登录]
 * @author ly
 * @version v1.0
 * Date:2017年8月11日 下午2:44:01
 */
public class CupmsGlobalFactory implements SessionFactory {

    @Override
    public Session createSession(SessionContext sessionContext) {
        CupmsGlobalSession session = new CupmsGlobalSession();
        if (null != sessionContext && sessionContext instanceof WebSessionContext) {
            WebSessionContext webSessionContext = (WebSessionContext) sessionContext;
            HttpServletRequest request = (HttpServletRequest) webSessionContext.getServletRequest();
            if (null != request) {
                session.setHost(request.getRemoteAddr());
                session.setUserAgent(request.getHeader("User-Agent"));
            }
        }
        return session;
    }

}
