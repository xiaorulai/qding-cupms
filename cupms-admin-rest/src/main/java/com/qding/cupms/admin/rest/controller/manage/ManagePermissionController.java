package com.qding.cupms.admin.rest.controller.manage;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

import com.qding.cupms.admin.rest.controller.BaseController;
import com.qding.cupms.common.model.AnswerMsg;
import com.qding.cupms.common.model.AnswerMsgFactoryImpl;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;


/**
 * <功能详细描述>
 * @since  [产品/模块版本]
 * @author Administrator
 * @version v1.0
 * Date:2017年8月10日 下午3:04:31
 */
@RestController
@Api(value = "权限管理", description = "权限管理")
@RequestMapping("/manage/permission")
public class ManagePermissionController extends BaseController {

    private static Logger _log = LoggerFactory.getLogger(ManagePermissionController.class);



    /** 
     * 权限列表
     * @return
     * @author Administrator
     * Date:2017年8月10日 下午3:05:39
     */
    @ApiOperation(value = "权限列表")
    @RequestMapping(value = "/list", method = RequestMethod.GET)
    @ResponseBody
    public AnswerMsg list() {
        return AnswerMsgFactoryImpl.createSuccessAnserMsg("OK");
        
    }

  

}
