package com.qding.cupms.admin.rest.shiro;

import org.apache.log4j.Logger;
import org.apache.shiro.cache.CacheManager;
import org.apache.shiro.session.Session;
import org.apache.shiro.session.SessionListener;
import org.apache.shiro.session.mgt.SimpleSession;


/**
 * 会话失败监听
 * 会话失败监听
 * @since  [基础数据/单点登录]
 * @author Administrator
 * @version v1.0
 * Date:2017年8月10日 下午7:02:11
 */
public class ShiroSessionListener implements SessionListener {
   Logger logger = Logger.getLogger(ShiroSessionListener.class);
//    private CacheManager cacheManager;
    @Override
    public void onStart(Session session) {
        logger.debug("session start " + session.getId());
    }

    @Override
    public void onStop(Session session) {
        logger.debug("session stop " + session.getId());
    }

    @Override
    public void onExpiration(Session session) {
        logger.info("session expiration "+session.getId());
    }

    // public void setCacheManager(CacheManager cacheManager) {
    // this.cacheManager = cacheManager;
    // }
}
