package com.qding.cupms.admin.rest.client;

import java.io.IOException;
import java.io.PrintWriter;

import javax.servlet.ServletResponse;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.qding.cupms.common.constant.Constant;
import com.qding.cupms.common.http.SimpleHttpParam;
import com.qding.cupms.common.http.SimpleHttpResult;
import com.qding.cupms.common.http.SimpleHttpUtils;
import com.qding.cupms.common.model.AnswerMsg;
import com.qding.cupms.common.model.AnswerMsgFactoryImpl;
import com.qding.cupms.common.model.ResponseCodeEnum;
import com.qding.cupms.common.model.SSOClientInfo;
import com.qding.cupms.common.utils.JsonUtil;

/**
 * 客户端代理类
 * 
 * @since [基础数据/单点登录]
 * @author ly
 * @version v1.0 Date:2017年8月11日 上午11:09:58
 */
public class ClientHelper {

    private final static Logger _log = LoggerFactory.getLogger(ClientHelper.class);
    
    
    /**
     * 向response 回写报文 向response 回写报文
     * 
     * @param response
     *            请求响应
     * @param answerMsg
     *            报文
     * @author ly Date:2017年8月10日 上午10:17:43
     * @throws IOException
     */
    public static void writeResponse(ServletResponse response, AnswerMsg answerMsg) throws IOException {
        PrintWriter out = null;
        try {
            String msg = JsonUtil.Java2Json(answerMsg);
            response.setCharacterEncoding(Constant.RESPONSE_CHARACTER_ENCODING);
            response.setContentType(Constant.RESPONSE_CONTENTTYPE);
            out = response.getWriter();
            out.write(msg);
            out.flush();
        } finally {
            if (out != null) {
                out.close();
            }
        }

    }

    /**
     * 未登录响应报文
     * @param response
     * @throws IOException
     * @author Administrator Date:2017年8月10日 上午11:27:55
     */
    public static boolean writeNologinResponse(ServletResponse response) throws IOException {
        PrintWriter out = null;
        try {
            AnswerMsg answerMsg = AnswerMsgFactoryImpl.createErrorAnserMsg(ResponseCodeEnum.AUTH.getValue(), "请先登录");
            String msg = JsonUtil.Java2Json(answerMsg);
            response.setCharacterEncoding(Constant.RESPONSE_CHARACTER_ENCODING);
            response.setContentType(Constant.RESPONSE_CONTENTTYPE);
            out = response.getWriter();
            out.write(msg);
            out.flush();
        } finally {
            if (out != null) {
                out.close();
            }
        }

        return true;

    }
    
    /**
     * 系统异常响应报文
     * 
     * @param response
     * @throws IOException
     * @author Administrator Date:2017年8月10日 上午11:27:55
     */
    public static boolean writeSSOSysErrorResponse(ServletResponse response) throws IOException {
        PrintWriter out = null;
        try {
            AnswerMsg answerMsg = AnswerMsgFactoryImpl.createErrorAnserMsg(ResponseCodeEnum.SYSTEM_ERROR.getValue(), "SSO系统异常");
            String msg = JsonUtil.Java2Json(answerMsg);
            response.setCharacterEncoding(Constant.RESPONSE_CHARACTER_ENCODING);
            response.setContentType(Constant.RESPONSE_CONTENTTYPE);
            out = response.getWriter();
            out.write(msg);
            out.flush();
        } finally {
            if (out != null) {
                out.close();
            }
        }

        return true;

    }

    /**
     * 客户端登出
     * @param clientInfo
     *            客户端系统
     * @param token
     *            令牌
     * @return
     * @author ly Date:2017年8月11日 上午11:57:57
     */
    public static boolean logout(SSOClientInfo clientInfo, String token) {
        try {
            // 获取客户端登录完整地址
            String realUrl = SimpleHttpUtils.createUrlAddress(clientInfo.getServerUri(), clientInfo.getLogoutUri());
            SimpleHttpParam httpParam = new SimpleHttpParam(realUrl);
            httpParam.addHeader(Constant.HEADER_TOKEN_NAME, token);
            _log.info("url:{},header:{}", realUrl, httpParam.getHeaders());

            SimpleHttpResult httpResult = SimpleHttpUtils.httpRequest(httpParam);
            _log.info("httpResult:{}", httpResult.toString());
            return true;
        } catch (Exception e) {
            _log.error(e.getMessage(), e);
            return false;
        }

    }

}
