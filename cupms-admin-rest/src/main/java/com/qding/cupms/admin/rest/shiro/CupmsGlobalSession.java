package com.qding.cupms.admin.rest.shiro;

import java.io.Serializable;

import org.apache.shiro.session.mgt.SimpleSession;

/**
 * 全局session  
 * @since  [基础数据/单点登录]
 * @author ly
 * @version v1.0
 * Date:2017年8月11日 下午2:43:00
 */
public class CupmsGlobalSession extends SimpleSession implements  Serializable{
    
    private static final long serialVersionUID = -7125642695178165650L;

    public static enum OnlineStatus {
        on_line("在线"), off_line("离线"), force_logout("强制退出");
        private final String info;

        private OnlineStatus(String info) {
            this.info = info;
        }

        public String getInfo() {
            return info;
        }
    }

    // 用户浏览器类型
    private String userAgent;

    // 在线状态
    private OnlineStatus status = OnlineStatus.off_line;

    public String getUserAgent() {
        return userAgent;
    }

    public void setUserAgent(String userAgent) {
        this.userAgent = userAgent;
    }

    public OnlineStatus getStatus() {
        return status;
    }

    public void setStatus(OnlineStatus status) {
        this.status = status;
    }

    @Override
    public String toString() {
        return "CupmsGlobalSession [getUserAgent()=" + getUserAgent() + ", getStatus()=" + getStatus() + ", getId()="
                + getId() + ", getStartTimestamp()=" + getStartTimestamp() + ", getStopTimestamp()="
                + getStopTimestamp() + ", getLastAccessTime()=" + getLastAccessTime() + ", isExpired()=" + isExpired()
                + ", getTimeout()=" + getTimeout() + ", getHost()=" + getHost() + ", getAttributes()=" + getAttributes()
                + ", isStopped()=" + isStopped() + ", isValid()=" + isValid() + ", isTimedOut()=" + isTimedOut()
                + ", getAttributeKeys()=" + getAttributeKeys() + ", hashCode()=" + hashCode() + ", toString()="
                + super.toString() + ", getClass()=" + getClass() + "]";
    }
    
    
    

}
