package com.qding.cupms.common.constant;


/**
 * <一句话功能简述>
 * 用到的一些常量
 * @since  [产品/模块版本]
 * @author ly
 * @version v1.0
 * Date:2017年8月10日 上午11:12:12
 */
public class Constant {
    
    /**
     * HEADER_TOKEN_NAME  token在 header中的名称
     */
    public final static String HEADER_TOKEN_NAME="token"; 
    
    
    
    /***************client********************************/
    
    /**
     * RESPONSE_CHARACTER_ENCODING  响应编码
     */
    public final static String  RESPONSE_CHARACTER_ENCODING="UTF-8";
    
    /**
     * RESPONSE_CONTENTTYPE  响应  ContentType  类型
     */
    public final static String RESPONSE_CONTENTTYPE="application/json; charset=utf-8";
    
    
    /**
     * TOKEN_REDIS_HASH_KEY  client端   token到session的映射关系
     */
    public final static String CLIENT_TOKEN_REDIS_HASH_KEY="token_redis_token2session";
    
    /**
     * CLIENT_SESSSION_REIDS_TAG client 端局部会话key
     */
    public final static String CLIENT_SESSSION_REIDS_TAG = "cupms_sso_client_session";
    
    public final static String  CLIENT_LOG_SIGN="userSign";
    public final static String  CLIENT_USER_SIGN="userInfo";
    
    public final static String  CLIENT_PROPERTIES="cupms-sso-client";
    
    public final static String   CLIENT_PROPERTIES_SERVERURI ="cupms.sso.server.uri";
    /***************end********************************/
    
    
    
    /***************server*********************************************/
    /**
     * GLOBAL_SESSION_USER_KEY 全局会话  用户信息key
     */
    public final static String GLOBAL_SESSION_USER_KEY="SSOUSERINFO";
    
    
    public final static String  SERVER_SSO_CHECKTOKEN="/SSO/checkToken";
    
    public final static String  SERVER_SSO_CANCEL="/SSO/cancel";
    
    /**
     * GLOBAL_SESSION_CLIENT_SYS_KEY  全局会话  已登录过的客户端系统
     */
    public final static String GLOBAL_SESSION_CLIENT_SYS_KEY="CLIENTSYS";
    /***************end*********************************************/

}
