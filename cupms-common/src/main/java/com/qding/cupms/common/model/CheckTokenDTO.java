package com.qding.cupms.common.model;

import java.io.Serializable;

public class CheckTokenDTO implements Serializable {

	private static final long serialVersionUID = -8548359485701489594L;

	/**
	 * @Field @statu : 状态
	 */
	private String statu;

	/**
	 * @Field @userInfo : 用户信息
	 */
	private SSOUserinfo userInfo;

	/**
	 * @Field @powerStr : 权限字符串
	 */
	private String powerStr;

	public String getStatu() {
		return statu;
	}

	public void setStatu(String statu) {
		this.statu = statu;
	}

	public SSOUserinfo getUserInfo() {
		return userInfo;
	}

	public void setUserInfo(SSOUserinfo userInfo) {
		this.userInfo = userInfo;
	}

	public String getPowerStr() {
		return powerStr;
	}

	public void setPowerStr(String powerStr) {
		this.powerStr = powerStr;
	}

}
