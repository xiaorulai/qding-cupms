/**
 * @Title: Page.java
 * @Package com.qding.park.api.web.service.domain.response
 * @Description: TODO
 * Copyright: Copyright (c) 2011 
 * Company:千丁互联
 * 
 * @author Comsys-Administrator
 * @date 2016年8月19日 上午11:18:32
 * @version V1.0
 */

package com.qding.cupms.common.model;

/**
  * @ClassName: Page
  * @Description: TODO
  * @author Comsys-Administrator
  * @date 2016年8月19日 上午11:18:32
  *
  */

public class Page {
	private int currentPage;
	
	private int numPerPage;
	
	private int totalCount;
	
	private int hast;
	
	private int next;
}
