package com.qding.cupms.common.model;

import java.io.Serializable;

/**
 * 
 * 单点登录系统客户端
 * @since  [基础数据/单点登录]
 * @author Administrator
 * @version v1.0
 * Date:2017年8月10日 下午3:35:53
 */
public class SSOClientInfo implements Serializable{
    
    private static final long serialVersionUID = -8548359485701489594L;
    
    
    /**
     * serverUrl  客户端系统服务地址
     */
    private String serverUri;
    
    /**
     * logoutUri  注销地址
     */
    private String logoutUri;
    
    
    
    public String getServerUri() {
        return serverUri;
    }


    
    public void setServerUri(String serverUri) {
        this.serverUri = serverUri;
    }


    
    public String getLogoutUri() {
        return logoutUri;
    }


    
    public void setLogoutUri(String logoutUri) {
        this.logoutUri = logoutUri;
    }
    
}
