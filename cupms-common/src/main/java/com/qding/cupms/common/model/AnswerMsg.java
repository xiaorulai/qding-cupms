/**
 * @Title: AnswerMsg.java
 * @Package com.qding.park.api.web.service.domain.response
 * @Description: TODO
 * Copyright: Copyright (c) 2011 
 * Company:千丁互联
 * 
 * @author Comsys-Administrator
 * @date 2016年8月19日 上午10:25:59
 * @version V1.0
 */

package com.qding.cupms.common.model;

import com.alibaba.fastjson.annotation.JSONField;
import com.alibaba.fastjson.serializer.SerializerFeature;

/**
  * @ClassName: AnswerMsg
  * @Description: 响应报文
  * @author Comsys-Administrator
  * @date 2016年8月19日 上午10:25:59
  *
  */

public class AnswerMsg {
	/**
	  * @Fields code : 响应编号（用一句话描述这个变量表示什么）
	  */
	private int code;
	
	/**
	  * @Fields msg : 响应描述（用一句话描述这个变量表示什么）
	  */
//	@JsonInclude(value=Include.NON_NULL)
	private String msg="";
	
	/**
	  * @Fields data : 数据（用一句话描述这个变量表示什么）
	  */
//	@JsonInclude(value=Include.NON_NULL)
//	@JSONField(serialzeFeatures={SerializerFeature.WriteMapNullValue})
	private Object data;
	
	/**
	  * @Fields page : 分页信息（用一句话描述这个变量表示什么）
	  */
//	@JsonInclude(value=Include.NON_NULL)
//	@JSONField(serialzeFeatures={SerializerFeature.WriteMapNullValue})
//	private Page page;
	
	
	
	
	/**
	 * getter method
	 * @return the code
	 */
	
	public int getCode() {
		return code;
	}




	/**
	 * setter method
	 * @param code the code to set
	 */
	
	public void setCode(int code) {
		this.code = code;
	}




	/**
	 * getter method
	 * @return the msg
	 */
	
	public String getMsg() {
		return msg;
	}




	/**
	 * setter method
	 * @param msg the msg to set
	 */
	
	public void setMsg(String msg) {
		this.msg = msg;
	}




	/**
	 * getter method
	 * @return the data
	 */
	
	public Object getData() {
		return data;
	}




	/**
	 * setter method
	 * @param data the data to set
	 */
	
	public void setData(Object data) {
		this.data = data;
	}


	


//	/**
//	 * getter method
//	 * @return the page
//	 */
//	
//	public Page getPage() {
//		return page;
//	}
//
//
//
//
//	/**
//	 * setter method
//	 * @param page the page to set
//	 */
//	
//	public void setPage(Page page) {
//		this.page = page;
//	}




}
