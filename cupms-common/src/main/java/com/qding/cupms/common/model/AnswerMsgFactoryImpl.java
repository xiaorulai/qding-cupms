/**
 * @Title: AnswerMsgFactoryImpl.java
 * @Package com.qding.park.api.web.service.domain.response
 * @Description: TODO
 * Copyright: Copyright (c) 2011 
 * Company:千丁互联
 * 
 * @author Comsys-Administrator
 * @date 2016年8月19日 上午11:17:16
 * @version V1.0
 */

package com.qding.cupms.common.model;

import java.util.HashMap;


/**
 * @ClassName: AnswerMsgFactoryImpl
 * @Description: TODO
 * @author Comsys-Administrator
 * @date 2016年8月19日 上午11:17:16
 *
 */

public class AnswerMsgFactoryImpl {

	public static AnswerMsg createAnswerMsg(Object data, Page page) {

		AnswerMsg msg = new AnswerMsg();

		msg.setData(data);
		msg.setMsg("处理请求成功");
		return msg;

	}

	public static AnswerMsg createAnswerMsg(Object data) {

		AnswerMsg msg = new AnswerMsg();

		msg.setData(data);
		msg.setMsg("处理请求成功");
		return msg;

	}
	
	
	/**
	 * @Description (创建错误报文)
	 * @param errorCode  错误编码
	 * @param msg  错误消息
	 * @return
	 */
	public static AnswerMsg createErrorAnserMsg(int errorCode,String msg){
		AnswerMsg answer = new AnswerMsg();
		answer.setCode(errorCode);
		answer.setMsg(msg==null?"":msg);
		answer.setData(new HashMap<String,Object>());
		
		return answer;
	}
	
	
	/**
	 * @Description (创建错误报文)
	 * @param responseType  错误类型枚举
	 * @return
	 */
	public static AnswerMsg createErrorAnserMsg(ResponseCodeEnum responseType){
		AnswerMsg answer = new AnswerMsg();
		answer.setCode(responseType.getValue());
		answer.setMsg(responseType.getDesc());
		answer.setData(new HashMap<String,Object>());
		
		return answer;
	}
	
	public static AnswerMsg createSuccessAnserMsg(String msg){
		AnswerMsg answer = new AnswerMsg();
		answer.setCode(ResponseCodeEnum.SUCESS.getValue());
		answer.setMsg(msg);
		answer.setData(new HashMap<String,Object>());
		
		return answer;
	}
}
