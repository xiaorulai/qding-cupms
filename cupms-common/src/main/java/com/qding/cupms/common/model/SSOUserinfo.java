package com.qding.cupms.common.model;

import java.io.Serializable;

/**
 * 用户信息
 * 用户信息
 * @since  [基础数据/单点登录]
 * @author ly
 * @version v1.0
 * Date:2017年8月10日 下午3:24:40
 */
public class SSOUserinfo implements Serializable{
    
    private static final long serialVersionUID = -8548359485701489594L;
    /**
     * account  账号
     */
    private String  account;
    
    /**
     * password 密码
     */
    private String password;
    
    
    /**
     * mailbox  邮箱
     */
    private String  mailbox;
    
    /**
     * realName 真实姓名
     */
    private String  realName;
    
    /**
     * gender 性别
     */
    private String gender;
    
    /**
     * status 字符串
     */
    private String status;
    
    /**
     * tenantId 租户id
     */
    private String tenantId;
    
    /**
     * creator  创建人
     */
    private String creator;

    
    public String getAccount() {
        return account;
    }

    
    public void setAccount(String account) {
        this.account = account;
    }

    
    public String getPassword() {
        return password;
    }

    
    public void setPassword(String password) {
        this.password = password;
    }

    
    public String getMailbox() {
        return mailbox;
    }

    
    public void setMailbox(String mailbox) {
        this.mailbox = mailbox;
    }

    
    public String getRealName() {
        return realName;
    }

    
    public void setRealName(String realName) {
        this.realName = realName;
    }


    
    public String getGender() {
        return gender;
    }


    
    public void setGender(String gender) {
        this.gender = gender;
    }


    
    public String getStatus() {
        return status;
    }


    
    public void setStatus(String status) {
        this.status = status;
    }


    
    public String getTenantId() {
        return tenantId;
    }


    
    public void setTenantId(String tenantId) {
        this.tenantId = tenantId;
    }


    
    public String getCreator() {
        return creator;
    }


    
    public void setCreator(String creator) {
        this.creator = creator;
    }
    
}
