package com.qding.cupms.common.model;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * 系统会话 系统会话
 * 
 * @since [基础数据/单点登录]
 * @author ly
 * @version v1.0 Date:2017年8月10日 下午5:50:55
 */
public class SSOSystemInfo implements Serializable {

    private static final long serialVersionUID = -8548359485701489595L;

    private Map<String,SSOClientInfo> clientMap = new HashMap<String,SSOClientInfo>();
    
    
    private Date  updateTime=new Date();

    public Map<String,SSOClientInfo> getClientclientMap() {
        return clientMap;
    }

    
    public Date getUpdateTime() {
        return updateTime;
    }

    
    public void setUpdateTime(Date updateTime) {
        this.updateTime = updateTime;
    }

    
}
