package com.qding.cupms.common.enums;


/**
 * @ClassName ArkCarTypeEnum
 * @Description TODO(方舟车牌类型)
 * @author LeiYang
 * @Date 2017年5月3日 下午2:20:54
 * @version 1.0.0
 */
public enum TokenStatuEnum {
    
    LEGITIMATE("合法", "200"),
    WRONGFUL("不合法","401");
    
    /** 枚举值 */
    private String value;
    /** 描述 */
    private String desc;
    
    private TokenStatuEnum(String desc, String value) {
        this.value = value;
        this.desc = desc;
    }


    
    public String getValue() {
        return value;
    }


    
    public void setValue(String value) {
        this.value = value;
    }


    public String getDesc() {
        return desc;
    }

    
    public void setDesc(String desc) {
        this.desc = desc;
    }
    
    
    public static TokenStatuEnum getEnum(String value) {
        TokenStatuEnum resultEnum = null;
        TokenStatuEnum[] enumAry = TokenStatuEnum.values();
        for (int i = 0; i < enumAry.length; i++) {
            if (enumAry[i].getValue().equals(value)) {
                resultEnum = enumAry[i];
                break;
            }
        }
        return resultEnum;
    }

}
