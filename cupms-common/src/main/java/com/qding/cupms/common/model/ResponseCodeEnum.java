package com.qding.cupms.common.model;

/**
 * 日志中操作类型
 * 
 * @desc
 * @author shenjialong
 * @date 2014-2-14,上午10:29:50
 */
public enum ResponseCodeEnum {

	SUCESS("成功", 0),
	AUTH("账号认证未通过", 401),
	AUTHON("无权限", 403),
	SYSTEM_ERROR("系统极异常",8000);


	

	/** 描述 */
	private String desc;
	/** 枚举值 */
	private int value;

	private ResponseCodeEnum(String desc, int value) {
		this.desc = desc;
		this.value = value;
	}

	public int getValue() {
		return value;
	}

	public void setValue(int value) {
		this.value = value;
	}

	public String getDesc() {
		return desc;
	}

	public void setDesc(String desc) {
		this.desc = desc;
	}

}
