package com.qding.cupms.common.utils;

import java.util.Random;
import java.util.UUID;

/**
 * Created by JackyChang on 17/3/1.
 *
 */
public class RandomUtil {

    /**
     * 生成字母＋数字随机数
     * @param length
     * @return
     */
    public static String getCharAndNumr(int length) {
        String val = "";
        Random random = new Random();
        for (int i = 0; i < length; i++) {
            String charOrNum = random.nextInt(2) % 2 == 0 ? "char" : "num";
            if ("char".equalsIgnoreCase(charOrNum)) {
                int choice = random.nextInt(2) % 2 == 0 ? 65 : 97;
                val += (char) (choice + random.nextInt(26));
            } else if ("num".equalsIgnoreCase(charOrNum)) {
                val += String.valueOf(random.nextInt(10));
            }
        }
        return val;
    }

    public static void main(String[] args) {
        System.out.println(getCharAndNumr(6));
        System.out.println(UUID.randomUUID());
    }
}
