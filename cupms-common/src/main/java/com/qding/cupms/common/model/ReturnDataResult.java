package com.qding.cupms.common.model;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * @ClassName ParkDataResult
 * @Description TODO(车场rest 接口自定义响应报文 data部份)
 * @author Administrator
 * @Date 2016年8月23日 下午2:42:34
 * @version 1.0.0
 */
public class ReturnDataResult {
	private static final String LIST_TAG = "list";
	
	private static final String LIST_DATA = "data";
	
	/**
	 * SUCCESS_CODE  处理成功的编码
	 */
	public static final int  SUCCESS_CODE=0;
	
	/**
	 * FAIL_CODE 处理失败的编码
	 */
	public static final int FAIL_CODE=-1;
	
	/**
	 * msg 处理结果
	 */
	private String msg;
	
	/**
	 * code 处理是否成功
	 */
	private  int code;

	/**
	 * @Field @data : TODO(存放数据的map)
	 */
	private Map<String, Object> data = new HashMap<String, Object>();

	/**
	 * @Description (向datalist 加入列表)
	 * @param list
	 */
	public void putDataOnList(List list) {

		List datalist = (List) data.get(LIST_TAG);

		datalist.addAll(list);
	}

	
	public static ReturnDataResult createEmpty(){
		ReturnDataResult data=new ReturnDataResult();
		
		return data;
	}
	/**
	 * @Description (向datalist 加入单个对象)
	 * @param map
	 */
	public void putDataOnList(Map map) {

		List datalist = (List) data.get(LIST_TAG);

		datalist.add(map);
	}

	/**
	 * @Description (创建list DATA区域)
	 * @return
	 */
	public static ReturnDataResult createListDate() {
		ReturnDataResult data = new ReturnDataResult();

		data.getData().put(LIST_TAG, new ArrayList<Map<String, Object>>());

		return data;

	}
	
	/**
	 * @Description (创建obj类型数据)
	 * @return
	 */
	public static ReturnDataResult createObjDate() {
		ReturnDataResult dataResult = new ReturnDataResult();
		
		return dataResult;

	}
	
	public  void putDataOnObj(String key,Object value){
		data.put(key, value);
	}
	
	public  void putMapOnObj(Map map){
		data.putAll(map);
	}

	public Map<String, Object> getData() {
		return data;
	}


    
    public String getMsg() {
        return msg;
    }


    
    public void setMsg(String msg) {
        this.msg = msg;
    }


    
    public int getCode() {
        return code;
    }


    
    public void setCode(int code) {
        this.code = code;
    }
	
	
}
