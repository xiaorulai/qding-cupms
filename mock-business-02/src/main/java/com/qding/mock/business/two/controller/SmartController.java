package com.qding.mock.business.two.controller;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.log4j.Logger;
import org.apache.shiro.SecurityUtils;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import com.qding.cupms.common.model.AnswerMsg;
import com.qding.cupms.common.model.AnswerMsgFactoryImpl;
import com.qding.cupms.common.model.ResponseCodeEnum;
import com.qding.cupms.common.model.ReturnDataResult;

import io.swagger.annotations.Api;

@RestController
@Api(value = "门禁测试", description = "门禁测试")
public class SmartController {

    private Logger _log = Logger.getLogger(SmartController.class);

    @RequestMapping(value = "/index", method = RequestMethod.POST)
    public AnswerMsg index(String account, String password, HttpServletRequest request, HttpServletResponse response) {

     return AnswerMsgFactoryImpl.createSuccessAnserMsg("/index>>>>>>OK");

    }
    
    @RequestMapping(value = "/device/insert", method = RequestMethod.POST)
    public AnswerMsg device_insert(String account, String password, HttpServletRequest request, HttpServletResponse response) {

     return AnswerMsgFactoryImpl.createSuccessAnserMsg("/device/insert>>>>>>OK");

    }

}
